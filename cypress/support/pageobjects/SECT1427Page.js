/// <reference types="Cypress" />

import SECT1427Elements from '../elements/SECT1427Elements'
const sect1427Elements = new SECT1427Elements
const url = Cypress.config("baseUrl_hotfix")

class SECT1427Page {

    impersonarFornecedor(fornecedor) {

        cy.visit(url)
        cy.restoreLocalStorage()
        cy.login()
        cy.saveLocalStorage()

        cy.get(sect1427Elements.botaoImpersonar()).click()
        cy.get(sect1427Elements.botaoDefinirParticipante()).click()
        cy.get(sect1427Elements.clicarNoInputDefinirParticipante()).click()
        cy.get(sect1427Elements.inputDefinirParticipante()).type(fornecedor)
        cy.wait(2000)
        cy.get(sect1427Elements.inputDefinirParticipante()).type('{Enter}')
        cy.wait(1500)
        cy.get(sect1427Elements.botaoDefinir()).click({force:true})
    }

    habilitarParametroDoProduto() {

        cy.get(sect1427Elements.botaoConfiguracaoMenu()).click()
        cy.get(sect1427Elements.botaoParametroDoProduto()).click()
        cy.get(sect1427Elements.botaoNovoParametro()).click()
        
        cy.get('div').contains('Selecione um produto').click()
        cy.get('li').contains('Cessão').click({force : true})
        cy.get('div').contains('Selecione um tipo').click()
        cy.get('li').contains('Produto').click()

        if(cy.get(sect1427Elements.checkboxDemonstrarRepresentanteComercial()).should('not.be.checked')){
            cy.get(sect1427Elements.checkboxDemonstrarRepresentanteComercial()).click()
        }else{
            cy.get(sect1427Elements.checkboxDemonstrarRepresentanteComercial()).should('be.checked')
        }
    }

    verificarVinculados() {

    cy.get(sect1427Elements.botaoFecharParametro()).click()
    cy.get(sect1427Elements.botaoEstabelecimento()).click()
    cy.get(sect1427Elements.botaoVinculados()).click()

    }

    retornarColunaDoRepresentanteComercial() {

        cy.get('span').contains('Disponível para Cessão')
    }

}

export default SECT1427Page;