/// <reference types="Cypress" />

import SECT1434Elements from '../elements/SECT1434Elements'
const sect1434Elements = new SECT1434Elements
const url = Cypress.config("baseUrl_hotfix")

class SECT1434Page {

    impersonar(impersonei) {
        cy.visit(url)
        cy.restoreLocalStorage()
        cy.login()
        cy.saveLocalStorage()

        cy.get(sect1434Elements.botaoImpersonar()).click()
        cy.get(sect1434Elements.botaoDefinirParticipante()).click()
        cy.get(sect1434Elements.clicarNoInputDefinirParticipante()).click()
        cy.get(sect1434Elements.inputDefinirParticipante()).type(impersonei)
        cy.wait(2000)
        cy.get(sect1434Elements.inputDefinirParticipante()).type('{Enter}')
        cy.get(sect1434Elements.botaoDefinir()).click()
    }

    definirParametros(){

        cy.wait(3000)
        cy.get(sect1434Elements.botaoConfiguracao()).click()
        cy.wait(2000)
        cy.get(sect1434Elements.botaoParametroDeProduto).contains('Parâmetro de Produto').click()
        cy.wait(3000)
        cy.get(sect1434Elements.botaoNovoParametro).contains('Novo Parâmetro').click()

        cy.wait(3000)
        cy.get(sect1434Elements.campoProduto).contains('Selecione um produto').click()
        cy.get('li').contains('Cessão').click({force:true})

        cy.get(sect1434Elements.campoTipo).contains('Selecione um tipo').click()
        cy.get('li').contains('Vinculo').click()
    }

    definirParticipanteFN(Fornecedor) {

       cy.get('nz-select[class="ng-tns-c21-11 ant-select ant-select-enabled ant-select-allow-clear ng-pristine ng-valid ng-star-inserted ng-touched"]').click()
       //cy.get('nz-select[class="ng-tns-c21-11 ant-select ant-select-enabled ant-select-allow-clear ng-pristine ng-valid ng-star-inserted ng-touched ant-select-open"]').type(Fornecedor)
    }

    definirParticipanteEC(Estabelecimento){

        cy.get('nz-form-control').click()
        
    }

    habilitarParametro() {

        if (cy.get(sect1434Elements.checkboxOcultarTelaDeVinculo).should('not.be.checked')) {
            cy.get(sect1434Elements.checkboxOcultarTelaDeVinculo).click()
            cy.get(sect1434Elements.salvarParametro).click()
        }
    }

    acessarListaVinculados() {

        cy.get(sect1434Elements.fecharParametro).click()
        cy.get(sect1434Elements.botaoEstabelecimentos).click()
        cy.get(sect1434Elements.botaoVinculados).click()
    }

    verificarVinculadoRemovido(participante) {

        cy.get('div').contains(participante).log('Participante ocultado com sucesso')
    }

    desabilitarParametro() {

        if (cy.get(sect1434Elements.checkboxOcultarTelaDeVinculo).should('be.checked')) {
            cy.get(sect1434Elements.checkboxOcultarTelaDeVinculo).click()
            cy.get(sect1434Elements.salvarParametro).click()
        }
    }

    verificarVinculadoNaoRemovido(participante) {

        cy.get('div').contains(participante).log('Participante não foi ocultado')
    }

}

export default SECT1434Page;