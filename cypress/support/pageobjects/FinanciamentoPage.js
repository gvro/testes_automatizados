/// <reference types="Cypress" />

import FinanciamentoElements from '../elements/FinanciamentoElements'
const financiamentoElements = new FinanciamentoElements
const url = Cypress.config("baseUrl_dev")

class FinanciamentoPage {

    acessarSite() {
        cy.visit(url)
        cy.restoreLocalStorage()
        cy.login()
        cy.saveLocalStorage()
    }

    acessarFinanciamentoBackOffice() {
        cy.wait(1500)
        cy.get(financiamentoElements.botaoConfiguracaoMenu()).click()
        cy.get(financiamentoElements.botaoConcultaDeClientesMenu()).click()
    }

    definirUsuarioAtivo(usuarios1) {
        cy.wait(1500)
        cy.get(financiamentoElements.inputNomeConsultaDeClientes()).type(usuarios1)
        cy.get(financiamentoElements.botaoPesquisarCliente()).click()
        cy.wait(1500)
        cy.get(financiamentoElements.botaoParametro()).click()
    }

    verificarFinanciamentoAtivo() {
        cy.wait(1500)
        cy.get(financiamentoElements.checkboxFinanciamentoAtivo()).should('be.checked')
    }

    definirUsuarioDasativado(usuarios2){
        cy.wait(1500)
        cy.get(financiamentoElements.inputNomeConsultaDeClientes()).type(usuarios2)
        cy.get(financiamentoElements.botaoPesquisarCliente()).click()
        cy.wait(1500)
        cy.get(financiamentoElements.botaoParametro()).click()
    }

    verificarFinanciamentoDesativado() {
        cy.wait(1500)
        cy.get(financiamentoElements.checkboxFinanciamentoDesativado()).should('not.be.checked')
    }

}

export default FinanciamentoPage;