/// <reference types="Cypress" />

import CERC1d2Elements from '../elements/CERC1d2Elements'
const cerc1d2Elements = new CERC1d2Elements
const url = Cypress.config("baseUrl_dev")

class CERC1d2Page {

    acessarTelaDeCadastro() {
        cy.visit(url)
        cy.restoreLocalStorage()
        cy.login()
        cy.saveLocalStorage()

        cy.get('.menu > .menu-bottom > .parent:nth-child(2) > .item > .fa-angle-right').click()
        cy.get('.menu-bottom > .active > .children > .item:nth-child(1) > .text').click()
    }

    cadastrarEstabelecimento() {

        if(cy.get('.mt-2').should('not.be.checked')){
            cy.get('.mt-2').click()
        }


    }

    retornarAlerta() {

    }
}

export default CERC1d2Page;