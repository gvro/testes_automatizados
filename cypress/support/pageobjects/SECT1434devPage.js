/// <reference types="Cypress" />

import SECT1434devElements from '../elements/SECT1434devElements'
const sect1434devElements = new SECT1434devElements
const url = Cypress.config("baseUrl_dev")

class SECT1434devPage {

    impersonar(impersonei) {
        cy.visit(url)
        cy.restoreLocalStorage()
        cy.login()
        cy.saveLocalStorage()

        cy.get(sect1434devElements.botaoImpersonar()).click()
        cy.get(sect1434devElements.botaoDefinirParticipante()).click()
        cy.get(sect1434devElements.clicarNoInputDefinirParticipante()).click()
        cy.get(sect1434devElements.inputDefinirParticipante()).type(impersonei)
        cy.wait(2000)
        cy.get(sect1434devElements.inputDefinirParticipante()).type('{Enter}')
        cy.get(sect1434devElements.botaoDefinir()).click()
    }

    definirParametros() {

        cy.wait(3000)
        cy.get(sect1434devElements.botaoConfiguracao()).click()
        cy.wait(2000)
        cy.get(sect1434devElements.botaoParametroDeProduto).contains('Parâmetro de Produto').click()
        cy.wait(3000)
        cy.get(sect1434devElements.botaoNovoParametro).contains('Novo Parâmetro').click()

        cy.wait(3000)
        cy.get(sect1434devElements.campoProduto).contains('Selecione um produto').click()
        cy.get('li').contains('Cessão').click({ force: true })

        cy.get(sect1434devElements.campoTipo).contains('Selecione um tipo').click()
        cy.get('li').contains('Vinculo').click()
    }

    definirParticipanteFN(Fornecedor) {

        cy.get('div[class="ant-select-selection__rendered"]').contains('Digite o nome do fornecedor').click()
        cy.get('div[class="ant-select-selection__rendered"]').find('input').type(Fornecedor)
        cy.get('li[class="ant-select-dropdown-menu-item ng-star-inserted"]').click()
    }

    definirParticipanteEC(Estabelecimento) {

        cy.get('div[class="ant-select-selection__rendered"]').contains('Digite o nome do estabelecimento').click()
        cy.get('.ant-select-selection > .ant-select-selection__rendered > .ant-select-search:nth-child(2) > .ant-select-search__field__wrap > .ant-select-search__field').type('yand')
        cy.get('li[class="ant-select-dropdown-menu-item ng-star-inserted"]').contains(' YANDEH S/A').click()
    }

    habilitarParametro() {

        cy.get('label[class="ant-checkbox-wrapper ng-valid ng-dirty ng-touched"]').find('input').click()
    }

    acessarListaVinculados() {

        cy.get(sect1434devElements.fecharParametro).click()
        cy.get(sect1434devElements.botaoEstabelecimentos).click()
        cy.get(sect1434devElements.botaoVinculados).click()
    }

    verificarVinculadoRemovido(participante) {

        cy.get('div').contains(participante).log('Participante ocultado com sucesso')
    }

    desabilitarParametro() {

        if (cy.get(sect1434devElements.checkboxOcultarTelaDeVinculo).should('be.checked')) {
            cy.get(sect1434devElements.checkboxOcultarTelaDeVinculo).click()
            cy.get(sect1434devElements.salvarParametro).click()
        }
    }

    verificarVinculadoNaoRemovido(participante) {

        cy.get('div').contains(participante).log('Participante não foi ocultado')
    }

}

export default SECT1434devPage;