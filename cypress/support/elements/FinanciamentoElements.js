class FinanciamentoElements {

    botaoConfiguracaoMenu = () => { return '.menu-bottom > :nth-child(3) > :nth-child(1)' }

    botaoConcultaDeClientesMenu = () => { return ':nth-child(3) > .children > :nth-child(1)' }

    inputNomeConsultaDeClientes = () => { return '#nome' }

    botaoPesquisarCliente = () => { return '.btn-primary' }

    botaoParametro = () => { return ':nth-child(1) > .cdk-column-buttons > .buttons-div-cell > .btn-outline-dark' }

    checkboxFinanciamentoAtivo = () => { return '#mat-checkbox-4-input' }

    checkboxFinanciamentoDesativado = () => { return '#mat-checkbox-8 > .mat-checkbox-layout > .mat-checkbox-inner-container' }
}

export default FinanciamentoElements;