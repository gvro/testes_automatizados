class SECT1427Elements {

    botaoConfiguracaoMenu = () => { return '.menu > .menu-bottom > .parent:nth-child(3) > .item > .text' }

    botaoParametroDoProduto = () => { return '.menu-bottom > .active > .children > .item:nth-child(2) > .text'}

    botaoNovoParametro = () => { return '.content-wrapper > .ng-star-inserted > .d-flex > .buttons > .btn'}

    botaoImpersonar = () => { return 'alpe-user-detail > .dropdown > #dropdown-user-details > .ng-star-inserted > span:nth-child(1)' }

    botaoDefinirParticipante = () => { return 'alpe-user-detail > .dropdown > .dropdown-menu > .user-body > .btn:nth-child(1)'}

    clicarNoInputDefinirParticipante = () => { return '.modal-content > .modal-body > .select2 > .selection > .select2-selection'}

    inputDefinirParticipante = () => { return '.background-alpe > .select2-container > .select2-dropdown > .select2-search > .select2-search__field'}

    botaoDefinir = () => { return '.modal > .modal-dialog > .modal-content > .modal-footer > .btn-primary'}

    botaoProduto = () => { return '.ant-form-item-children > .ng-tns-c21-3 > .ant-select-selection > .ant-select-selection__rendered > .ant-select-selection__placeholder'}

    produtoCessao = () => { return '#cdk-overlay-1 > .ant-select-dropdown > .ng-tns-c21-3 > .ant-select-dropdown-menu > .ant-select-dropdown-menu-item:nth-child(1)'}

    botaoTipo = () => { return '.ant-form-item-children > .ng-tns-c21-5 > .ant-select-selection > .ant-select-selection__rendered > .ant-select-selection__placeholder'}

    tipoProduto = () => { return '#cdk-overlay-2 > .ant-select-dropdown > .ng-tns-c21-5 > .ant-select-dropdown-menu > .ant-select-dropdown-menu-item:nth-child(1)'}

    checkboxDemonstrarRepresentanteComercial = () => { return '.ng-tns-c20-20 > .ant-form-item-control > .ant-form-item-children > .ant-checkbox-wrapper > .ant-checkbox > .ant-checkbox-input'}

    botaoFecharParametro = () => { return '.ant-drawer-header > .ant-drawer-close > .anticon > svg > path' }

    botaoEstabelecimento = () => { return '.menu > .menu-bottom > .parent:nth-child(9) > .item > .text' }

    botaoVinculados = () => { return '.menu-bottom > .parent:nth-child(9) > .children > .item:nth-child(2) > .text' }
}

export default SECT1427Elements;