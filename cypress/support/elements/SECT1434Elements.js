class SECT1434Elements {

    botaoImpersonar = () => { return 'alpe-user-detail > .dropdown > #dropdown-user-details > .ng-star-inserted > span:nth-child(1)' }

    botaoDefinirParticipante = () => { return 'alpe-user-detail > .dropdown > .dropdown-menu > .user-body > .btn:nth-child(1)'}

    clicarNoInputDefinirParticipante = () => { return '.modal-content > .modal-body > .select2 > .selection > .select2-selection'}

    inputDefinirParticipante = () => { return '.background-alpe > .select2-container > .select2-dropdown > .select2-search > .select2-search__field'}

    botaoDefinir = () => { return '.modal > .modal-dialog > .modal-content > .modal-footer > .btn-primary'}

    botaoConfiguracao = () => { return '.menu > .menu-bottom > .parent:nth-child(3) > .item > .text' }

    botaoParametroDeProduto = () => { return '.menu-bottom > .active > .children > .item:nth-child(2) > .text' }

    botaoNovoParametro = () => { return '.content-wrapper > .ng-star-inserted > .d-flex > .buttons > .btn' }

    campoProduto = () => { return '.ant-form-item-children > .ng-tns-c29-994 > .ant-select-selection > .ant-select-selection__rendered > .ant-select-selection__placeholder' }

    produtoCessao = () => { return '#cdk-overlay-105 > .ant-select-dropdown > .ng-tns-c29-1044 > .ant-select-dropdown-menu > .ant-select-dropdown-menu-item:nth-child(1)' }

    campoTipo = () => { return '.ant-form-item-control > .ant-form-item-children > .ng-tns-c29-873 > .ant-select-selection > .ant-select-selection__rendered' }

    tipoVinculo = () => { return '#cdk-overlay-96 > .ant-select-dropdown > .ng-tns-c29-873 > .ant-select-dropdown-menu > .ant-select-dropdown-menu-item:nth-child(3)' }

    participanteFN = () => { return '.ant-select-search ant-select-search--inline ng-tns-c24-12 ng-star-inserted' }
    
    participanteEC = () => { return '#cdk-overlay-1 > div > div.ant-drawer-content-wrapper > div > div > div.ant-drawer-body > form > div > div:nth-child(4) > nz-form-item > nz-form-control > div > span > nz-select > div > div > div.ant-select-selection__placeholder.ng-tns-c29-83.ng-star-inserted' }

    checkboxOcultarTelaDeVinculo = () => { return '.ng-tns-c36-890 > .ant-form-item-control > .ant-form-item-children > .ant-checkbox-wrapper > .ant-checkbox > .ant-checkbox-input' }

    salvarParametro = () => { return '.ant-drawer-content > .ant-drawer-wrapper-body > .ant-drawer-body > .footer > .ant-btn-primary' }

    fecharParametro = () => { return '.ant-drawer-wrapper-body > .ant-drawer-header > .ant-drawer-close > .anticon > svg' }

    botaoEstabelecimentos = () => { return '.menu > .menu-bottom > .parent:nth-child(9) > .item > .text' }

    botaoVinculados = () => { return '.menu-bottom > .parent:nth-child(9) > .children > .item:nth-child(2) > .text' }

}

export default SECT1434Elements;