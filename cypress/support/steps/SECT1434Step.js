/* global Given, Then, When, And */

import SECT1434Page from '../pageobjects/SECT1434Page'
const sect1434Page = new SECT1434Page

Given(/^"([^"]*)" como EC ou FN,$/, (impersonei) => {
    sect1434Page.impersonar(impersonei);
})

// When(/^defini os participantes "([^"]*)" e "([^"]*)",$/, (Fornecedor, Estabelecimento) => {
//     sect1434Page.definirParticipanteFN();
// })

When("definir os parâmetros,", () => {
    sect1434Page.definirParametros();
})

When(/^definir o participante "([^"]*)",$/, (Fornecedor) => {
    sect1434Page.definirParticipanteFN(Fornecedor);
})

When(/^e definir o participante "([^"]*)",$/, (Estabelecimento) => {
    sect1434Page.definirParticipanteEC(Estabelecimento);
})

When("habilitei o parâmetro (Ocultar na tela de Vinculo:),", () => {
    sect1434Page.habilitarParametro();
})

When("ir para a lista de vinculados,", () => {
    sect1434Page.acessarListaVinculados();
})

Then(/^na tela de vínculos o "([^"]*)" será removido da lista de vinculados.$/, (participante) => {
    sect1434Page.verificarVinculadoRemovido(participante);
})

When("não habilitei o parâmetro (Ocultar na tela de Vinculo:),", () => {
    sect1434Page.desabilitarParametro();
})

Then(/^na tela de vínculos o "([^"]*)" não será removido da lista de vinculados.$/, (participante) => {
    sect1434Page.verificarVinculadoNaoRemovido(participante);
})

