/* global Given, Then, When, And */

import SECT1434devPage from '../pageobjects/SECT1434devPage'
const sect1434devPage = new SECT1434devPage

Given(/^"([^"]*)" como EC ou FN,$/, (impersonei) => {
    sect1434devPage.impersonar(impersonei);
})

// When(/^defini os participantes "([^"]*)" e "([^"]*)",$/, (Fornecedor, Estabelecimento) => {
//     sect1434devPage.definirParticipanteFN();
// })

When("definir os parâmetros,", () => {
    sect1434devPage.definirParametros();
})

When(/^definir o participante "([^"]*)",$/, (Fornecedor) => {
    sect1434devPage.definirParticipanteFN(Fornecedor);
})

When(/^e definir o participante "([^"]*)",$/, (Estabelecimento) => {
    sect1434devPage.definirParticipanteEC(Estabelecimento);
})

When("habilitei o parâmetro Ocultar na tela de Vinculo:,", () => {
    sect1434devPage.habilitarParametro();
})

When("ir para a lista de vinculados,", () => {
    sect1434devPage.acessarListaVinculados();
})

Then(/^na tela de vínculos o "([^"]*)" será removido da lista de vinculados.$/, (participante) => {
    sect1434devPage.verificarVinculadoRemovido(participante);
})

When("não habilitei o parâmetro (Ocultar na tela de Vinculo:),", () => {
    sect1434devPage.desabilitarParametro();
})

Then(/^na tela de vínculos o "([^"]*)" não será removido da lista de vinculados.$/, (participante) => {
    sect1434devPage.verificarVinculadoNaoRemovido(participante);
})

