/* global Given, Then, When, And */

import CERC1d2Page from '../pageobjects/CERC1d2Page'
const cerc1d2Page = new CERC1d2Page

Given(/^eu estiver na tela de cadastro,$/, () => {
	cerc1d2Page.acessarTelaDeCadastro();
});

When(/^cadastrar o estabelecimento,$/, () => {
	cerc1d2Page.cadastrarEstabelecimento();
});

Then(/^o portal irá retornar um alerta que esse estabelecimento já existe,$/, () => {
	cerc1d2Page.retornarAlerta();
});
