/* global Given, Then, When, And */

import FinanciamentoPage from '../pageobjects/FinanciamentoPage'
const financiamentoPage = new FinanciamentoPage

import FinanciamentoElements from '../elements/FinanciamentoElements'
const financiamentoElements = new FinanciamentoElements
const url = Cypress.config("baseUrl_dev")

Given("esteja logado", () => {
    financiamentoPage.acessarSite();
})

When("acesso a página de configuração em Consultas de Clientes", () => {
    financiamentoPage.acessarFinanciamentoBackOffice() ;
})

When(/^busco o parâmetro do "([^"]*)" ativo$/, (usuarios1) => {
    financiamentoPage.definirUsuarioAtivo(usuarios1);
})

Then("financiamento deve estar ativo", () => {
    financiamentoPage.verificarFinanciamentoAtivo();
})

When(/^busco o parâmetro do "([^"]*)" desativado$/, (usuarios2) => {
    financiamentoPage.definirUsuarioDasativado(usuarios2);
})

Then("financiamento deve estar desativado", () => {
    financiamentoPage.verificarFinanciamentoDesativado();
})