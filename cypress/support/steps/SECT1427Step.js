/* global Given, Then, When, And */

import SECT1427Page from '../pageobjects/SECT1427Page'
const sect1427Page = new SECT1427Page


Given(/^impersonar um "([^"]*)",$/, (fornecedor) => {
	sect1427Page.impersonarFornecedor(fornecedor);
});

When("o parâmetro do produto Demonstrar representante comercial esteja habilitado,", () => {
    sect1427Page.habilitarParametroDoProduto();
});

When("verificar a tela de vinculados,", () => {
    sect1427Page.verificarVinculados();
});

Then("irá poder visualizar em uma coluna o representante comercial", () => {
    sect1427Page.retornarColunaDoRepresentanteComercial();
});