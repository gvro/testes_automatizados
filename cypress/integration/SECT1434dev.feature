Feature: Criar parâmetro para inibir exibição de Ec's indesejado
    Eu enquanto Alpe,
    Quero visualizar um novo parâmetro com o nome (Ocultar na tela de vinculados)
    Para remover o vínculo da lista na tela de (Vínculos)

    Scenario Outline: Verificar se o parâmetro ativo está ocultando vinculados
        Given "<impersonei>" como EC ou FN,
        When definir os parâmetros,
        And definir o participante "<Fornecedor>",
        And e definir o participante "<Estabelecimento>",
        And habilitei o parâmetro Ocultar na tela de Vinculo:,
        And ir para a lista de vinculados,
        Then na tela de vínculos o "<participante>" será removido da lista de vinculados.

        Examples:
            | impersonei | Fornecedor          | Estabelecimento  | participante        |
            | Fazenda    | Faze                | Yand             | Yandehh S/A         |
            | Yande      | Faze                | Yand             | Fazenda Paraíso S/A |

    # Scenario Outline: Verificar se o parâmetro desativado não está ocultando vinculados
    #     Given "<impersonei>" como EC ou FN,
    #     When definir os parâmetros,
    #     And definir o participante "<Fornecedor>",
    #     And e definir o participante "<Estabelecimento>",
    #     And não habilitei o parâmetro Ocultar na tela de Vinculo:,
    #     And ir para a lista de vinculados,
    #     Then na tela de vínculos o "<participante>" não será removido da lista de vinculados.

    #     Examples:
    #         | impersonei | Fornecedor          | Estabelecimento | participante        |
    #         | Fazenda    | Faze                | Yand            | Yandehh S/A                   |
    #         | Yande      | Faze                | Yand            | Fazenda Paraíso S/A |