Feature: Produto de financiamento
    Afim de cobrir mais os testes do produto de financiamento
    Como PO e QA
    Eu quero poder saber se o produto de financiamento do
    usuário está ativo ou desativado

    Scenario Outline: Verificar se EC possui financiamento ativo
        Given esteja logado
        When acesso a página de configuração em Consultas de Clientes
        And busco o parâmetro do "<usuarios1>" ativo
        Then financiamento deve estar ativo

    Examples:
    |usuarios1|
    |amarildo|
    |Camila Teste 1 Ltda|


    Scenario Outline: Verificar se EC possui financiamento desativado
        Given esteja logado
        When acesso a página de configuração em Consultas de Clientes
        And busco o parâmetro do "<usuarios2>" desativado
        Then financiamento deve estar desativado

    Examples:
    |usuarios2|
    |ferrari|
    |ERP Teste|