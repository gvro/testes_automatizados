Feature: Parâmetro do produto e vinculados
    Eu enquanto Alpe,
    Preciso visualizar em vinculados o representante comercial de cada EC,
    Para que o fornecedor visualize quais EC´s vieram pela sua indicação.

    Scenario Outline:
        Given impersonar um "<fornecedor>",
        When o parâmetro do produto Demonstrar representante comercial esteja habilitado,
        And verificar a tela de vinculados,
        Then irá poder visualizar em uma coluna o representante comercial

        Examples:
            | fornecedor |
            | Yande      |
            | Fazenda    |